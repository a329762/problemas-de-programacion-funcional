const idArray = [123,456,789], nameArray = ["coca.cola","pepsi","kwama"]
const desArray = ["azucar al por mil","azucar al por mil pero del sur","dulce nectar de los Dioses"]
const clasArray = ["refresco","refresco","shebe"], exisArray = [30,10,150], minArray = [12,3,30], priceArray = [17,14,15]
var listArray = [], high = 0, low = 0, list1 = 0, list2 = [], m = 0,n = 0, value1 = 0, value2 = 0

function makeList(idVal, nameVal, desVal, clasVal, exisVal, minimunVal, priceVal) {
  const objA = {
    id: idVal,
    name: nameVal,
    description: desVal,
    clasification: clasVal,
    existance: exisVal,
    minimun: minimunVal,
    price: priceVal
  };
  return objA;
}

function exisMaxMin (value){
  if(value > 20){
    high+=1
  } else if (value < 15) {
    low+=1
  }
  return high, low;
}

function clasPrice (clas, price, obj){
  if(clas == "refresco"){
    if(price > 15.5){
      list1 = obj;
    }
  } else if (clas == "shebe") {
    if(price > 15.5){
      list1 = obj;
    }
  }
  return "Misma categoría, precio mayor a 15.5: " + list1.name;
}

function priceFunction (price, obj){
  if(price > 20.30 && price < 45){
      list2 = obj;
  }
  return "precio mayor a 20.30 y menor a 45.00: " + list2.name;
}

function clasList (clas, price, obj){
  if(clas == "refresco"){
    value1++
  } else if (clas == "shebe") {
    value2++
  }
  return value1, value2;

}

for (var i = 0; i <= 2; i++){
    listArray[i] = makeList(idArray[i], nameArray[i], desArray[i], clasArray[i], exisArray[i], minArray[i], priceArray[i]);
    exisMaxMin(listArray[i].existance)
    clasList(listArray[i].clasification);
}

console.log("Listado de Articulos");
console.log(listArray);
console.log("Cantidad de Articulos mayor a 20: " + high + "  Cantidad de Articulos menor a 15: " + low);
console.log("Clasificacion refresco: " + value1 + " Clasificacion shebe: " + value2);
for (var j = 0; j <= 2; j++){
console.log(clasPrice(listArray[j].clasification, listArray[j].price, listArray[j]));
console.log(priceFunction(listArray[j].price, listArray[j]));
}
